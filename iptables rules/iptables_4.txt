General (to log dropped packets)

iptables -N LOGGING
iptables -A INPUT -j LOGGING
iptables -A LOGGING -j LOG --log-prefix "Dropped Packets: " --log-level 7
iptables -A LOGGING -j DROP

------------------------------------------------------------------------------------------------------------------------------


Router

ipset create LAN1 hash:ip
ipset create LAN2 hash:ip
ipset add LAN1 192.168.14.0/24
ipset add LAN1 192.168.6.0/24

iptables -t nat -A PREROUTING -i eth3 -m set --match-set LAN1 src -p tcp -j DNAT --to-destination 173.194.39.210
iptables -t nat -A PREROUTING -i eth2 -m set --match-set LAN2 src -p tcp -j DNAT --to-destination 173.194.39.210

iptables -t filter -A FORWARD -i eth3 -o eth1 -d 173.194.39.210 -p tcp -m state --state NEW,RELATED,ESTABLISHED -j ACCEPT
iptables -t filter -A FORWARD -i eth2 -o eth1 -d 173.194.39.210 -p tcp -m state --state NEW,RELATED,ESTABLISHED -j ACCEPT
iptables -t filter -A FORWARD -i eth1 -o eth3 -s 173.194.39.210 -p tcp -m state --state NEW,RELATED,ESTABLISHED -j ACCEPT
iptables -t filter -A FORWARD -i eth1 -o eth2 -s 173.194.39.210 -p tcp -m state --state NEW,RELATED,ESTABLISHED -j ACCEPT

iptables -t nat -A POSTROUTING -o eth1 -d 173.194.39.210 -p tcp -j SNAT --to 192.168.6.0-192.168.6.255
iptables -t nat -A POSTROUTING -o eth1 -d 173.194.39.210 -p tcp -j SNAT --to 192.168.14.0-192.168.14.255
iptables-save

------------------------------------------------------------------------------------------------------------------------------


COMPUTER 1

iptables -t filter -A INPUT -p tcp -s 173.194.39.210 -m state --state NEW,RELATED,ESTABLISHED -j ACCEPT
iptables -t filter -A OUTPUT -p tcp -d 173.194.39.210 -m state --state NEW,RELATED,ESTABLISHED -j ACCEPT
iptables-save

------------------------------------------------------------------------------------------------------------------------------


COMPUTER 2

iptables -t filter -A INPUT -p tcp -s 173.194.39.210 -m state --state NEW,RELATED,ESTABLISHED -j ACCEPT
iptables -t filter -A OUTPUT -p tcp -d 173.194.39.210 -m state --state NEW,RELATED,ESTABLISHED -j ACCEPT
iptables-save

------------------------------------------------------------------------------------------------------------------------------


COMPUTER 3

iptables -t filter -A INPUT -p tcp -s 173.194.39.210 -m state --state NEW,RELATED,ESTABLISHED -j ACCEPT
iptables -t filter -A OUTPUT -p tcp -d 173.194.39.210 -m state --state NEW,RELATED,ESTABLISHED -j ACCEPT
iptables-save

------------------------------------------------------------------------------------------------------------------------------


Google Machine

iptables -t filter -A INPUT -p tcp -s 193.140.236.80 -m state --state NEW,RELATED,ESTABLISHED -j ACCEPT
iptables -t filter -A OUTPUT -p tcp -d 193.140.236.80 -m state --state NEW,RELATED,ESTABLISHED -j ACCEPT
iptables-save

------------------------------------------------------------------------------------------------------------------------------



General (to delete old rules)

iptables -t filter -F
iptables -t nat -F
iptables -P INPUT DROP
iptables -P OUTPUT DROP
iptables -P FORWARD DROP

------------------------------------------------------------------------------------------------------------------------------